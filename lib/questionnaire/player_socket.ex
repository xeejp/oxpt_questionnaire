defmodule Oxpt.Questionnaire.PlayerSocket do
  use Cizen.Automaton
  defstruct [:game_id, :guest_id]

  use Cizen.Effects
  require Logger

  alias Cizen.{Dispatcher, Event, Filter}
  alias Oxpt.Player.{Input, Output}
  alias Oxpt.Questionnaire.{Message, MessageTo}

  @impl true
  def spawn(id, %__MODULE__{} = socket) do
    perform id, %Subscribe{
      event_filter: Filter.new(fn %Event{body: %Input{
        game_id: ^socket.game_id,
        guest_id: ^socket.guest_id
      }} -> true end)
    }

    perform id, %Subscribe{
      event_filter: Filter.new(fn %Event{body: %Message{
        game_id: ^socket.game_id,
      }} -> true end)
    }

    perform id, %Subscribe{
      event_filter: Filter.new(fn %Event{body: %MessageTo{
        game_id: ^socket.game_id,
        to_id: ^socket.guest_id,
      }} -> true end)
    }

    Logger.debug("PlayerSocket Spawned")
    Logger.debug(id)

    {:loop, socket}
  end

  @impl true
  def yield(id, {:loop, socket}) do
    event = perform id, %Receive{
      event_filter: Filter.new(fn
        %Event{body: %Input{}} -> true
        %Event{body: %Message{}} -> true
        %Event{body: %MessageTo{}} -> true
      end)
    }

    case event.body do
      %Input{event: "update_state", payload: payload} ->
        perform id, %Dispatch{
          body: %MessageTo{
            game_id: socket.game_id,
            to_id: socket.game_id,
            from_id: socket.guest_id,
            event: "update_state",
            payload: payload
          }
        }

      %Input{event: "fetch_state", payload: %{}} ->
        perform id, %Dispatch{
          body: %MessageTo{
            game_id: socket.game_id,
            to_id: socket.game_id,
            from_id: socket.guest_id,
            event: "fetch_state",
            payload: %{}
          }
        }

      %Message{event: "dispatch_state", payload: payload} ->
        perform id, %Dispatch{
          body: %Output{
            game_id: socket.game_id,
            guest_id: socket.guest_id,
            event: "update",
            payload: %{type: "dispatch_state", data: payload }
          }
        }

      %MessageTo{event: "dispatch_state", payload: payload} ->
        perform id, %Dispatch{
          body: %Output{
            game_id: socket.game_id,
            guest_id: socket.guest_id,
            event: "update",
            payload: %{type: "dispatch_state", data: payload }
          }
        }
    end

    {:loop, socket}
  end
end
