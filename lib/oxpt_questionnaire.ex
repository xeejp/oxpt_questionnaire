defmodule Oxpt.Questionnaire.Message do
  @enforce_keys [:game_id, :event, :payload]
  defstruct [:game_id, :event, :payload]
end

defmodule Oxpt.Questionnaire.MessageTo do
  @enforce_keys [:game_id, :to_id, :from_id, :event, :payload]
  defstruct [:game_id, :to_id, :from_id, :event, :payload]
end

defmodule Oxpt.Questionnaire do
  use Cizen.Automaton
  defstruct [:room_id]

  use Cizen.Effects
  alias Cizen.{Dispatcher, Event, Filter}
  alias Oxpt.JoinGame
  alias Oxpt.Questionnaire.Host
  alias Oxpt.Game
  alias Oxpt.Player.{Input, Output}
  alias Oxpt.Questionnaire.{Message, MessageTo}
  use Oxpt.Game,
    root_dir: Path.join(__ENV__.file, "../..") |> Path.expand(),
    index: "src/index.jsx",
    oxpt_path: "../oxpt"

  require Logger

  @impl Oxpt.Game
  def player_socket(game_id, game, guest_id) do
    %__MODULE__.PlayerSocket{game_id: game_id, guest_id: guest_id}
  end

  @impl Oxpt.Game
  def new(room_id, _params) do
    %__MODULE__{room_id: room_id}
  end

  @impl Oxpt.Game
  def metadata(),
    do: %{
      label: "label_questionnaire",
      category: "category_questionnaire"
    }

  @impl true
  def spawn(id, %__MODULE__{room_id: room_id}) do
    Logger.debug("Questionnaire Spawned")
    Logger.debug(id)

    host = perform id, %Start{
      saga: Host.new(room_id, [questionnaire_id: id])
    }

    perform id, %Subscribe{
      event_filter: Filter.new(fn
        %Event{body: %JoinGame{game_id: ^id}} -> true
        %Event{body: %MessageTo{game_id: ^id, to_id: ^id}} -> true
      end),
    }


    initial_state = %{
      common: %{
        state: "Playing", # ["Waiting", "Playing", "Finished"]
        start_page: "description",
        questionnaire: %{
          title: "Beauty Contest Game",
          description: "Sample",
          page_order: [
            :description,
            :game,
            :finish,
          ],
          pages: %{
            description: %{
              title: "Instruction",
              description: "Instruction Page",
              queries: [
                %{
                  _key: "agreement",
                  title: "Agreement",
                  description: "Do you agree to join this game?",
                  type: "selective",
                  redirect: true,
                  attributes: %{
                    type: "radio",
                    shuffle: false,
                    level: 2,
                    alternatives: [
                      %{
                        label: "Agree",
                        type: "text",
                        redirect: "game"
                      },
                      %{
                        label: "Disagree",
                        type: "text",
                        redirect: "finish"
                      }
                    ]
                  }
                }
              ]
            },
            game: %{
              title: "Experiment",
              description: "Winners will get prize moeny.",
              queries: [
                %{
                  _key: "contact",
                  title: "Contact",
                  description: "Contact e-mail address if you win.",
                  type: "descriptive",
                  attributes: %{
                    type: "text",
                    form: %{
                      label: "Student ID number",
                      placeholder: "5 digits",
                    },
                    type: "text"
                  }
                },
                %{
                  _key: "game",
                  title: "Beauty Contest Game",
                  description: "Enter integer number between 0 and 100. Winner will be the person who entered the number of 2/3 of average.",
                  type: "selective",
                  attributes: %{
                    type: "number",
                    min: 0,
                    max: 100,
                  }
                }
              ]
            },
            finish: %{
              title: "Experiment Finished",
              description: "Thank you.",
              queries: [
                %{
                  _key: "evaluation",
                  title: "Evaluation",
                  description: "Please evaluate this experiment between 1 (terrible) to 5 (excellent).",
                  type: "selective",
                  attributes: %{
                    type: "scale",
                    level: 5,
                    alternatives: [
                      %{
                        label: "Terrible"
                      },
                      %{
                        label: "Bad"
                      },
                      %{
                        label: "Normal"
                      },
                      %{
                        label: "Good"
                      },
                      %{
                        label: "Excellent"
                      }
                    ]
                  }
                },
                %{
                  _key: "option",
                  title: "Your information sources.",
                  description: "Which do you usually use? (Multiple answer)",
                  type: "selective",
                  attributes: %{
                    type: "check",
                    level: 4,
                    alternatives: [
                      %{
                        label: "News papers"
                      },
                      %{
                        label: "TV"
                      },
                      %{
                        label: "Web pages"
                      },
                      %{
                        label: "SNS"
                      }
                    ]
                  }
                }
              ]
            }
          }
        }
      },
      private: %{
      }
    }

    {:loop, host, initial_state}
  end

  @impl true
  def yield(id, {:loop, host, state}) do
    event = perform id, %Receive{}

    Logger.debug("questionnaire event handled")
    IO.inspect(event)

    {new_host, new_state} = case event.body do
      %JoinGame{host: true, guest_id: guest_id} ->
        perform id, %Request{
          body: %JoinGame{game_id: host, guest_id: guest_id, host: true}
        }
        {host, state}

      %JoinGame{host: false, guest_id: guest_id} ->
        currentData = Map.pop(state.private, guest_id, %{
          page: "",
          finished: false,
          answer: %{}
        }) |> elem(0)
        nextState = Map.put(state, :private, Map.put(state.private, guest_id, currentData))
        perform id, %Dispatch{
          body: %MessageTo{
            game_id: host,
            to_id: host,
            from_id: id,
            event: "dispatch_state_host",
            payload: Map.put(nextState.common, :guests, nextState.private)
          }
        }
        {host, nextState}

      # Fetch state by player_socket
      %MessageTo{event: "fetch_state", from_id: from_id} ->
        currentData = Map.pop(state.private, from_id, %{
          page: "",
          finished: false,
          answer: %{}
        }) |> elem(0)
        perform id, %Dispatch{
          body: %MessageTo{
            game_id: id,
            to_id: from_id,
            from_id: id,
            event: "dispatch_state",
            payload: Map.merge(state.common, currentData)
          }
        }
        {host, state}

      # Update state by player_socket
      %MessageTo{event: "update_state", from_id: from_id, payload: payload} ->
        nextState = Map.put(state, :private, Map.put(state.private, from_id, payload))
        perform id, %Dispatch{
          body: %MessageTo{
            game_id: id,
            to_id: from_id,
            from_id: id,
            event: "dispatch_state",
            payload: payload
          }
        }
        perform id, %Dispatch{
          body: %MessageTo{
            game_id: host,
            to_id: host,
            from_id: id,
            event: "dispatch_state_host",
            payload: Map.put(nextState.common, :guests, nextState.private)
          }
        }
        {host, nextState}

      # Fetch state by host
      %MessageTo{event: "fetch_state_host", from_id: from_id} ->
        perform id, %Dispatch{
          body: %MessageTo{
            game_id: host,
            to_id: from_id,
            from_id: id,
            event: "dispatch_state_host",
            payload: Map.put(state.common, :guests, state.private)
          }
        }
        {host, state}

      # Update state by host
      %MessageTo{event: "update_state_host", from_id: from_id, payload: payload} ->
        merged = Map.merge(state.common, payload)
        perform id, %Dispatch{
          body: %Message{
            game_id: id,
            event: "dispatch_state",
            payload: %{type: "dispatch_state", data: merged}
          }
        }
        perform id, %Dispatch{
          body: %MessageTo{
            game_id: host,
            to_id: from_id,
            from_id: id,
            event: "dispatch_state_host",
            payload: Map.put(merged, :guests, state.private)
          }
        }
        {host, Map.put(state, :common, merged)}

      _ ->
        Logger.debug("Event Unhundled")
        {host, state}
    end
    {:loop, new_host, new_state}
  end
end
