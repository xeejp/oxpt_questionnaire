// update local state
export const NEXT_PAGE = 'next_page'
export const BACK_PAGE = 'back_page'
export const FINISH = 'finish'

export const REDIRECT_TO = 'redirect_to'
export const UPDATE_FIELD = 'update_field'

// update server state
export const UPDATE_STATE = 'update_state'
export const FETCH_STATE = 'fetch_state'
export const DISPATCH_STATE = 'dispatch_state'

export const nextPage = page => ({ type: NEXT_PAGE, data: { page: page }})
export const backPage = page => ({ type: BACK_PAGE, data: { page: page }})
export const finish = () => ({ type: FINISH })
export const redirectTo = to => ({ type: REDIRECT_TO, data: { to: to }})
export const updateField = (key, value) => ({ type: UPDATE_FIELD, data: { key: key, value: value }})
export const updateState = () => ({ type: UPDATE_STATE })
export const fetchState = () => ({ type: FETCH_STATE })
