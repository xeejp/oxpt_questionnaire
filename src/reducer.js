import { NEXT_PAGE, BACK_PAGE, FINISH, REDIRECT_TO, UPDATE_FIELD, FETCH_STATE, DISPATCH_STATE } from './actions'

const initialState = {
  isFetching: false,
  finished: false,
  state: 'Waiting',
  start_page: '',
  page: '',
  questionnaire: {
  },
  answer: {
  }
}

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case NEXT_PAGE: {
      const pages = state.questionnaire.page_order
      const curIdx = Math.max(pages.indexOf(action.data.page), 0)
      const nextIdx = Math.min(curIdx + 1, Math.max(pages.length - 1, 0))
      return Object.assign({}, state, { page: pages[nextIdx] })
    }

    case BACK_PAGE: {
      const pages = state.questionnaire.page_order
      const curIdx = Math.max(pages.indexOf(action.data.page), 0)
      const prevIdx = Math.max(curIdx - 1, 0)
      return Object.assign({}, state, { page: pages[prevIdx] })
    }

    case FINISH:
      return Object.assign({}, state, { finished: true })

    case REDIRECT_TO:
      return Object.assign({}, state, { page: action.data.to !== '' ? action.data.to : state.questionnaire.page_order.slice(-1)[0] })

    case UPDATE_FIELD:
      return Object.assign({}, state, { answer: Object.assign({}, state.answer, { [action.data.key]: action.data.value }) })

    case FETCH_STATE:
      return Object.assign({}, state, { isFetching: true })

    case DISPATCH_STATE: {
      const nextState = Object.assign({}, action.data, { page: action.data.page || state.start_page })
      return Object.assign({}, state, { ...nextState, isFetching: false })
    }

    default:
      return state
  }
}
