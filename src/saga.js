import 'babel-polyfill'
import { take, fork, select } from 'redux-saga/effects'

import { UPDATE_STATE, FETCH_STATE } from './actions'

function * updateState() {
  while (true) {
    yield take(UPDATE_STATE)
    const state = yield select()
    const payload = {
      page: state.page,
      answer: state.answer,
      finished: state.finished,
    }
    sendData(UPDATE_STATE, payload)
  }
}

function * fetchState() {
  while (true) {
    yield take(FETCH_STATE)
    sendData(FETCH_STATE, {})
  }
}

export default function * saga() {
  yield fork(updateState)
  yield fork(fetchState)
}
