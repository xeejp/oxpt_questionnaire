import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import TextField from '@material-ui/core/TextField'
import FormLabel from '@material-ui/core/FormLabel'
import FormControl from '@material-ui/core/FormControl'
import FormHelperText from '@material-ui/core/FormHelperText'

import { updateField } from '../actions'

const mapStateToProps = (state) => ({})
const mapDispatchToProps = (dispatch) => ({
  UpdateField: (key, value) => dispatch(updateField(key, value)),
})

class DescriptiveForm extends Component {
  constructor(props) {
    super(props)
    this.state = {
      value: ''
    }
    this.handleChange = this.handleChange.bind(this)
  }

  componentDidMount() {
    this.setState({
      value: this.props.value
    })
  }

  handleChange(e) {
    const { _key, UpdateField } = this.props
    this.setState({ value: e.target.value })
    UpdateField(_key, e.target.value)
  }

  render() {
    const { title, description, required, form } = this.props

    switch (form.type) {
      case 'text':
        return <Fragment>
          <FormControl>
            <FormLabel>{title}</FormLabel>
            <FormHelperText>{description}</FormHelperText>
            <TextField
              label={form.label}
              required={required}
              placeholder={form.placeholder}
              onChange={this.handleChange}
              value={this.state.value}
            />
          </FormControl>
        </Fragment>

      case 'multiline':
        return <Fragment>
          <FormControl>
            <FormLabel>{title}</FormLabel>
            <FormHelperText>{description}</FormHelperText>
            <TextField
              label={form.label}
              required={required}
              multiline
              placeholder={form.placeholder}
              onChange={this.handleChange}
              value={this.state.value}
            />
          </FormControl>
        </Fragment>

      case 'number':
        return <Fragment>
          <FormControl>
            <FormLabel>{title}</FormLabel>
            <FormHelperText>{description}</FormHelperText>
            <TextField
              label={form.label}
              required={required}
              type='number'
              placeholder={form.placeholder}
              onChange={this.handleChange}
              value={this.state.value}
            />
          </FormControl>
        </Fragment>

      case 'secret':
        return <Fragment>
          <FormControl>
            <FormLabel>{title}</FormLabel>
            <FormHelperText>{description}</FormHelperText>
            <TextField
              label={form.label}
              required={required}
              type='password'
              placeholder={form.placeholder}
              onChange={this.handleChange}
              value={this.state.value}
            />
          </FormControl>
        </Fragment>

      default:
        return null
    }
  }
}

DescriptiveForm.propTypes = {
  _key: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  required: PropTypes.bool,
  form: PropTypes.shape({
    label: PropTypes.string.isRequired,
    placeholder: PropTypes.string,
  }).isRequired,
  type: PropTypes.oneOf(['text', 'multiline', 'number', 'password']).isRequired,
  value: PropTypes.any.isRequired,

  UpdateField: PropTypes.func.isRequired
}

DescriptiveForm.defaultProps = {
  description: '',
  required: false,
}

export default connect(mapStateToProps, mapDispatchToProps)(DescriptiveForm)
