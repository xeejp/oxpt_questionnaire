import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import SelectiveForm from './SelectiveForm'
import DescriptiveForm from './DescriptiveForm'

const mapStateToProps = (state) => ({})
const mapDispatchToProps = (dispatch) => ({})

class Query extends Component {
  render() {
    const { _key, title, description, type, required, redirect, attributes, value, onChangeRedirectDestination } = this.props
    return <Fragment>
      {type === 'selective' &&
        <SelectiveForm
          _key={_key}
          title={title}
          description={description}
          required={required}
          redirect={redirect}
          attributes={attributes}
          value={value}
          onChangeRedirectDestination={onChangeRedirectDestination}
        />
      }
      {type === 'descriptive' &&
        <DescriptiveForm
          _key={_key}
          title={title}
          description={description}
          required={required}
          form={attributes.form}
          type={attributes.type}
          value={value}
        />
      }
    </Fragment>
  }
}

Query.propTypes = {
  _key: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  type: PropTypes.oneOf(['selective', 'descriptive']).isRequired,
  required: PropTypes.bool,
  redirect: PropTypes.bool,
  attributes: PropTypes.object.isRequired,
  value: PropTypes.any.isRequired,
  onChangeRedirectDestination: PropTypes.func.isRequired,
}

Query.defaultProps = {
  description: '',
  required: false,
  redirect: false
}

export default connect(mapStateToProps, mapDispatchToProps)(Query)
