import React, { Component, Fragment } from 'react'
import styled from 'styled-components'

import CircularProgress from '@material-ui/core/CircularProgress'

const StyledCircularProgress = styled(CircularProgress)`
  margin: 10px;
`
export default class CircularDeterminate extends Component {
  constructor(props) {
    super(props)
    this.state = {
      completed: 0,
    }
    this.progress = this.progress.bind(this)
  }

  componentDidMount() {
    this.timer = setInterval(this.progress, 20)
  }

  componentWillUnmount() {
    clearInterval(this.timer)
  }

  progress() {
    const { completed } = this.state
    this.setState({ completed: completed >= 100 ? 0 : completed + 1 })
  }

  render() {
    return (
      <Fragment>
        <center>
          <StyledCircularProgress
            variant="determinate"
            value={this.state.completed}
          />
        </center>
      </Fragment>
    )
  }
}
