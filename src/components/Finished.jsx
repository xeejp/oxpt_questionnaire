import React, { Component } from 'react'
import styled from 'styled-components'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'

import CircularDeterminate from './CircularDeterminate'

const StyledCard = styled(Card)`
  width: 80%;
  padding: 20px 4%;
  margin: 10px 0;
`
export default class Finished extends Component {
  render() {
    const title = '終了'
    const description = '以上でアンケートは終了です．ご協力ありがとうございました．他の参加者が終了するのを待っています．'
    return <div>
      <StyledCard>
        <CardContent>
          <Typography variant='h5'>{title}</Typography>
          <Typography variant='subtitle1' color='textSecondary'>{description}</Typography>
          <CircularDeterminate />
        </CardContent>
      </StyledCard>
    </div>
  }
}
