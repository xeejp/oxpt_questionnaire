import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styled from 'styled-components'

import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import Divider from '@material-ui/core/Divider'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'

import { redirectTo, nextPage, finish, updateState } from '../actions'

import Query from './Query'

const mapStateToProps = ({ page, answer }) => ({
  page: page,
  answer: answer,
})

const mapDispatchToProps = (dispatch) => ({
  RedirectTo: (to) => dispatch(redirectTo(to)),
  NextPage: (page) => dispatch(nextPage(page)),
  Finish: () => dispatch(finish()),
  UpdateState: () => dispatch(updateState()),
})

const StyledCard = styled(Card)`
  width: 80%;
  padding: 20px 4%;
  margin: 10px 0;
`
const StyledDivider = styled(Divider)`
  &&{
    margin: 10px 0 20px 0;
  }
`
const Container = styled.div`
  margin: 20px 0 0 40px;
`

class Page extends Component {
  constructor(props) {
    super(props)
    this.state = {
      forRedirect: ''
    }
    this.handleChangeRedirectDestination = this.handleChangeRedirectDestination.bind(this)
    this.clearRedirectDestination = this.clearRedirectDestination.bind(this)
  }

  handleChangeRedirectDestination(dest) {
    this.setState({ forRedirect: dest })
  }

  clearRedirectDestination() {
    this.setState({ forRedirect: '' })
  }

  render() {
    const { title, description, queries, answer, isLast } = this.props
    const queryList = queries.map(e => <Fragment key={e._key}>
      <Query {...e}
        value={Object.keys(answer).includes(e._key) ? answer[e._key] : ''}
        onChangeRedirectDestination={this.handleChangeRedirectDestination}
      />
      <StyledDivider />
    </Fragment>)

    return <div>
      <StyledCard>
        <CardContent>
          <Typography variant='h5'>{title}</Typography>
          <Typography variant='subtitle1' color='textSecondary'>{description}</Typography>
          {((queries instanceof Array && queries.length !== 0) &&
            <Fragment>
              <StyledDivider />
              <Container>{queryList}</Container>
            </Fragment>
          )}
        </CardContent>
        <CardActions>
          {isLast
            ? <Button
              color='secondary'
              onClick={() => {
                if (this.state.forRedirect && this.state.forRedirect !== '')
                  this.props.RedirectTo(this.state.forRedirect)
                else
                  this.props.Finish()
                this.props.UpdateState()
                this.clearRedirectDestination()
              }}
            >Finish</Button>
            : <Button
              color='primary'
              onClick={() => {
                if (this.state.forRedirect && this.state.forRedirect !== '')
                  this.props.RedirectTo(this.state.forRedirect)
                else
                  this.props.NextPage(this.props.page)
                this.props.UpdateState()
                this.clearRedirectDestination()
              }}
            >Next</Button>
          }
        </CardActions>
      </StyledCard>
    </div>
  }
}

Page.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  queries: PropTypes.arrayOf(PropTypes.object).isRequired,
  isLast: PropTypes.bool.isRequired,

  answer: PropTypes.object,
  page: PropTypes.string.isRequired,

  RedirectTo: PropTypes.func.isRequired,
  NextPage: PropTypes.func.isRequired,
  Finish: PropTypes.func.isRequired,
  UpdateState: PropTypes.func.isRequired,
}

Page.defaultProps = {
  description: ''
}

export default connect(mapStateToProps, mapDispatchToProps)(Page)
