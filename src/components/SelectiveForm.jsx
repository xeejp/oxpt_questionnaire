import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import Typography from '@material-ui/core/Typography'
import Radio from '@material-ui/core/Radio'
import RadioGroup from '@material-ui/core/RadioGroup'
import MenuItem from '@material-ui/core/MenuItem'
import Checkbox from '@material-ui/core/Checkbox'
import Select from '@material-ui/core/Select'
import FormLabel from '@material-ui/core/FormLabel'
import FormGroup from '@material-ui/core/FormGroup'
import FormControl from '@material-ui/core/FormControl'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormControlLabel from '@material-ui/core/FormControlLabel'
import Slider from '@material-ui/core/Slider'

import { updateField } from '../actions'

const mapStateToProps = (state) => ({})
const mapDispatchToProps = (dispatch) => ({
  UpdateField: (key, value) => dispatch(updateField(key, value))
})

class SelectiveForm extends Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)
    this.handleChangeCheck = this.handleChangeCheck.bind(this)
    this.handleChangeSlide = this.handleChangeSlide.bind(this)
  }

  handleChange(e) {
    const { _key, UpdateField } = this.props
    UpdateField(_key, e.target.value)
  }

  handleChangeCheck(value) {
    const { _key, UpdateField } = this.props

    if (this.props.value instanceof Array) {
      if (this.props.value.includes(value)) {
        UpdateField(_key, this.props.value.filter(i => i !== value))
      } else {
        UpdateField(_key, this.props.value.concat([value]))
      }
    } else {
      UpdateField(_key, [value])
    }
  }

  handleChangeSlide(e, value) {
    const { _key } = this.props
    this.props.UpdateField(_key, value)
  }

  render() {
    const { title, description, required, redirect, attributes, value } = this.props

    switch (attributes.type) {
      case 'radio':
        return <Fragment>
          <FormControl required={required}>
            <FormLabel>{title}</FormLabel>
            <FormHelperText>{description}</FormHelperText>
            <RadioGroup
              value={value}
              onChange={(e) => {
                this.handleChange(e)
                if (redirect) {
                  let to = attributes.alternatives.find(x => x.label === e.target.value).redirect || ''
                  this.props.onChangeRedirectDestination(to)
                }
              }}
            >
              {attributes.alternatives.map(e =>
                <FormControlLabel
                  key={title + e.label}
                  value={e.label}
                  label={e.label}
                  control={<Radio />}
                />
              )}
            </RadioGroup>
          </FormControl>
        </Fragment>

      case 'check':
        return <Fragment>
          <FormControl>
            <FormLabel>{title}</FormLabel>
            <FormHelperText>{description}</FormHelperText>
            <FormGroup>
              {attributes.alternatives.map(e =>
                <FormControlLabel
                  key={title + e.label}
                  label={e.label}
                  control={
                    <Checkbox
                      checked={value instanceof Array && value.includes(e.label)}
                      onChange={() => this.handleChangeCheck(e.label)}
                      value={e.label}
                    />
                  }
                />
              )}
            </FormGroup>
          </FormControl>
        </Fragment>

      case 'select':
        return <Fragment>
          <FormControl required={required}>
            <FormLabel>{title}</FormLabel>
            <Select
              value={value}
              onChange={this.handleChange}
            >
              {attributes.alternatives.map(e =>
                <MenuItem
                  key={title + e.label}
                  value={e.label}
                >
                  {e.label}
                </MenuItem>
              )}
            </Select>
            <FormHelperText>{description}</FormHelperText>
          </FormControl>
        </Fragment>

      case 'number': {
        const items = [...Array(attributes.max - attributes.min)].map((x, i) =>
          <MenuItem
            key={title + i + attributes.min}
            value={i + attributes.min}
          >
            {i + attributes.min}
          </MenuItem>
        )
        return <Fragment>
          <FormControl required={required}>
            <FormLabel>{title}</FormLabel>
            <Select
              value={value}
              onChange={this.handleChange}
            >
              {items}
            </Select>
            <FormHelperText>{description}</FormHelperText>
          </FormControl>
        </Fragment>
      }

      case 'scale':
        return <Fragment>
          <Typography variant='subtitle1'>{title}</Typography>
          <Typography variant='subtitle2'>{description}</Typography>
          <Slider
            min={0}
            max={attributes.level - 1}
            step={1}
            value={Number(value)}
            marks={attributes.alternatives.map((x, i) => ({ value: i, label: x.label }))}
            onChange={this.handleChangeSlide}
          />
        </Fragment>

      default:
        return null
    }
  }
}

SelectiveForm.propTypes = {
  _key: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  required: PropTypes.bool,
  redirect: PropTypes.bool,
  attributes: PropTypes.oneOfType([
    PropTypes.shape({
      type: PropTypes.oneOf(['radio', 'check', 'select']).isRequired,
      shuffle: PropTypes.bool,
      alternatives: PropTypes.Array
    }),
    PropTypes.shape({
      type: PropTypes.oneOf(['number']).isRequired,
      min: PropTypes.number.isRequired,
      max: PropTypes.number.isRequired,
    }),
    PropTypes.shape({
      type: PropTypes.oneOf(['scale']).isRequired,
      level: PropTypes.number.isRequired,
      alternatives: PropTypes.arrayOf(PropTypes.object)
    })
  ]).isRequired,
  value: PropTypes.any.isRequired,
  onChangeRedirectDestination: PropTypes.func.isRequired,

  UpdateField: PropTypes.func.isRequired,
}

SelectiveForm.defaultProps = {
  description: '',
  required: false,
  redirect: false,
}

export default connect(mapStateToProps, mapDispatchToProps)(SelectiveForm)
