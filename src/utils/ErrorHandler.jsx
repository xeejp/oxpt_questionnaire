import React, { Component } from 'react'

export default class ErrorHandler extends Component {
  constructor(props) {
    super(props)
    this.state = {
      hasError: false
    }
  }

  componentDidCatch(_error, info) {
    this.setState({ hasError: true })
    // logging
    // console.log(error)
  }

  render() {
    if (this.setState.hasError) {
      return <h1>Something went wrong.</h1>
    }

    return this.props.children
  }
}
