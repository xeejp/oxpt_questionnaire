import React from 'react'
import i18n from '../i18n'

export default class Frame extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      count: 0,
      iFrameHeight: 0 + 'px',
    }
    this.receiveMessage = this.receiveMessage.bind(this)
  }

  _isMounted = false

  componentDidMount() {
    this._isMounted = true
    if (this._isMounted) window.addEventListener('message', this.receiveMessage, false)
  }

  componentWillUnmount() {
    if (this._isMounted) window.removeEventListener('message', this.receiveMessage, false)
    this._isMounted = false
  }

  receiveMessage(event) {
    if (this.state.iFrameHeight !== event.data.iFrameHeight) {
      var regex = /\d+px/
      if (regex.test(event.data.iFrameHeight)) {
        this.setState({ iFrameHeight: event.data.iFrameHeight })
      }
    }
    if ((event.data.lang) && (event.data.lang !== this.props.i18n.language)) {
      i18n.changeLanguage(event.data.lang)
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.iFrameHeight !== document.body.scrollHeight + 'px') {
      var nextState = { iFrameHeight: document.body.scrollHeight + 'px' }
      this.handleStateUpdate(nextState)
    }
  }

  handleStateUpdate(state) {
    window.parent.postMessage(state, '*')
  }

  render() {
    return (
      <div id='frameContent'>
        <div>
          {this.props.children}
        </div>
      </div>)
  }
}
