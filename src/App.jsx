/* eslint-disable camelcase */
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styled from 'styled-components'
import Typography from '@material-ui/core/Typography'

import Waiting from './components/Waiting'
import Finished from './components/Finished'
import Page from './components/Page'

import { fetchState } from './actions'

const Container = styled.div`
  width: 90%;
  margin: 0 auto;
`

const TextContainer = styled.div`
  width: 88%;
`

const PageCountTypography = styled(Typography)`
  float: right;
`

const mapStateToProps = (state) => state
const mapDispatchToProps = (dispatch) => ({
  FetchState: () => { dispatch(fetchState()) },
})

class App extends Component {
  componentDidMount() {
    this.props.FetchState()
  }

  render() {
    const { state, start_page, page, finished, questionnaire } = this.props
    const totalPage = (questionnaire && questionnaire.page_order) ? questionnaire.page_order.length : 0
    const currentPage = (questionnaire && questionnaire.page_order) ? Math.max(questionnaire.page_order.indexOf(page), 0) + 1 : 1
    return (
      <div>
        <Container>
          <TextContainer>
            <Typography variant='h5'>{questionnaire.title}</Typography>
            <Typography variant='subtitle1' color='textSecondary' style={{ display: 'inline' }}>{questionnaire.description}</Typography>
            {totalPage !== 0 && <PageCountTypography variant='subtitle1' color='textSecondary'>{currentPage}/{totalPage}</PageCountTypography>}
          </TextContainer>
          {!finished && state === 'Waiting' && <Waiting />}
          {!finished && state === 'Playing' &&
          <Page
            title={questionnaire.pages[page || start_page].title}
            description={questionnaire.pages[page || start_page].description}
            queries={questionnaire.pages[page || start_page].queries}
            isLast={currentPage === totalPage}
          />}
          {finished && <Finished />}
        </Container>
      </div>
    )
  }
}

App.propTypes = {
  state: PropTypes.string.isRequired,
  start_page: PropTypes.string.isRequired,
  page: PropTypes.string.isRequired,
  finished: PropTypes.bool.isRequired,
  questionnaire: PropTypes.object.isRequired,

  FetchState: PropTypes.func.isRequired,
}

export default connect(mapStateToProps, mapDispatchToProps)(App)
