defmodule Oxpt.Questionnaire.MixProject do
  use Mix.Project

  def project do
    [
      app: :oxpt_questionnaire,
      version: "0.1.0",
      build_path: "../../_build",
      config_path: "../../config/config.exs",
      deps_path: "../../deps",
      lockfile: "../../mix.lock",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:cizen, "~> 0.15.0"},
      {:oxpt, in_umbrella: true},
      {:oxpt_questionnaire_host, in_umbrella: true, override: true}
    ]
  end
end
